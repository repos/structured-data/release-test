# release-test
Test the release & deployment workflow of data pipelines.

## Release workflow
We follow Data Engineering's [workflow_utils](https://gitlab.wikimedia.org/repos/data-engineering/workflow_utils/-/blob/main/gitlab_ci_templates/README.md#project-versioning):
- the `main` branch is on a `.dev` release
- releases are made by removing the `.dev` suffix and committing a tag

## How to deploy
1. On the left sidebar, go to **CI/CD > Pipelines**
2. click on the _play_ button and select `trigger_release`
3. on the left sidebar, go to **Packages and registries > Package Registry**
4. click on the first item, right-click the asset file name, and copy the URL
5. branch out of [airflow-dags](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags)
6. update the URL in the [DAG config](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/platform_eng/config/artifacts.yaml)
7. merge into `main`
4. deploy the DAGs:
```
me@my_local_machine:~$ ssh deployment.eqiad.wmnet
me@deploy1002:~$ cd /srv/deployment/airflow-dags/platform_eng/
me@deploy1002:~$ scap deploy
```
See the [docs](https://gitlab.wikimedia.org/repos/data-engineering/workflow_utils/-/blob/main/gitlab_ci_templates/README.md#example-pipeline-usage) for more details.

# -*- coding: utf-8 -*-

from setuptools import find_packages, setup

setup(
    name='release-test',
    packages=find_packages(),
    version='0.2.0.dev',
    description='test the release workflow',
    author='mfossati',
)
